# [Web Responsive Restaurante](https://andrea-code.gitlab.io/basic-restaurant-template/)
![Demo Web](https://user-images.githubusercontent.com/37070594/47860513-fbba9a00-ddf0-11e8-978e-c5d5bf4cd540.gif)

Plantilla realizada a partir de un ejercicio el cual consistia:
- Enlaces interactivos con JQuery
- Agregar el efecto Parallax
- Implementar Google Maps
- Maquetación
- Estilos con SASS

### [Visualizar la pagina](https://andrea-code.gitlab.io/basic-restaurant-template/)


#### Recursos - Creditos:
- Fotografias: [Foodies Feed](https://www.foodiesfeed.com/)
  
